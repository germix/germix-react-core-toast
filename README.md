# Germix React Core - Timeline

## About

Germix react core timeline component

## Installation

```bash
npm install @germix/germix-react-core-timeline
```

## Build

```bash
npm run build
```

## Publish

```bash
npm publish
```

## Build & Publish

```bash
npm run build-publish
```
