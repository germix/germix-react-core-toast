import React from 'react';
import { uuid } from '@germix/germix-react-core';

interface ToastData
{
    id?,
    icon?,
    color?,
    title?,
    message?,
    timeout?,
    fullBackground?,

    status? : 'hidden'|'leaving',
}

export interface PropsWithToasts
{
    addToast,
    removeToast,
}

interface ToastProps
{
    toast: ToastData,
    onDismiss(),
    showToast(id),
    killToast(id),
    removeToast(id),
}

/**
 * @author Germán Martínez
 */
class Toast extends React.Component<ToastProps,
{
}>
{
    render()
    {
        let className = 'toast';
        
        if(this.props.toast.status)
            className += ' ' + this.props.toast.status;
        if(this.props.toast.color)
            className += ' color-' + this.props.toast.color;
        if(this.props.toast.fullBackground)
            className += ' full-background';

        return (
<div
className={className}
onTransitionEnd={(event) =>
{
    if(this.props.toast.status === 'leaving')
    {
        this.props.killToast(this.props.toast.id);
    }
}}
>
    <div className="toast-left-wrapper">
        { this.props.toast.icon &&
            <i className={this.props.toast.icon} />
        }
    </div>
    <div className="toast-content-wrapper">

        <div className="toast-content-title">
            { this.props.toast.title }
        </div>

        <div className="toast-content-message">
            { this.props.toast.message }
        </div>

    </div>
    <div className="toast-close-wrapper">
        { this.props.toast.status !== 'leaving' &&
        <span onClick={() =>
        {
            this.props.removeToast(this.props.toast.id);
        }} >x</span>
        }
    </div>
</div>
        );
    }

    componentDidMount()
    {
        setTimeout(() =>
        {
            this.props.showToast(this.props.toast.id);
        }, 20);
    }
};

interface ToastContextState
{
    toasts: ToastData[]
}

const ToastContext = React.createContext({
});

export const ToastContextConsumer = ({ children }) =>
(
    <ToastContext.Consumer>
        {context => children(context)}
    </ToastContext.Consumer>
);

export class ToastContextProvider extends React.Component<any,any>
{
    state: ToastContextState =
    {
        toasts: [],
    }
    render()
    {
        const { addToast, removeToast } = this;

        return (
<>
    <ToastContext.Provider value={{ addToast, removeToast }}>
        { this.props.children }
        <div style={{
            position: 'fixed',
            top: 0,
            right: 0,
            margin: 8,
            zIndex: 1000,

            overflowX: 'hidden',
            overflowY: 'hidden',
        }} >

            { this.state.toasts.map((toast) => {
                    return <Toast
                        key={toast.id}
                        toast={toast}
                        onDismiss={() =>
                        {
                            this.removeToast(toast.id);
                        }}
                        showToast={this.showToast}
                        killToast={this.killToast}
                        removeToast={this.removeToast}
                    />;
                })
            }
        </div>
    </ToastContext.Provider>
</>
        );
    }

    addToast = (toast: ToastData) =>
    {
        const id = uuid();

        this.setState({
            toasts: [
                ...this.state.toasts,
                {
                    ...toast,
                    id: id,
                    status: 'hidden',
                },
        ]});
        if(toast.timeout !== undefined)
        {
            setTimeout(() =>
            {
                this.removeToast(id);
            }, toast.timeout);
        }
    }

    showToast = (id) =>
    {
        this.setState({
            toasts: this.state.toasts.map((t: ToastData) =>
            {
                if(t.id == id)
                {
                    return {
                        ...t,
                        status: ''
                    };
                }
                return t;
            })
        });
    }

    killToast = (id) =>
    {
        this.setState({
            toasts: this.state.toasts.filter(t => t.id !== id)
        });
    }

    removeToast = (id) =>
    {
        this.setState({
            toasts: this.state.toasts.map((t: ToastData) =>
            {
                if(t.id == id)
                {
                    return {
                        ...t,
                        status: 'leaving'
                    };
                }
                return t;
            })
        });
    }
}

export const withToastsContext = (Comp: React.ComponentType<any>) =>
    React.forwardRef((props: any, ref: React.Ref<any>) => 
    (
        <ToastContextConsumer>
            { context => <Comp
                ref={ref}
                {...props}
                addToast={context.addToast}
                removeToast={context.removeToast}
                />
            }
        </ToastContextConsumer>
    )
);
